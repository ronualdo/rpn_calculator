require 'bigdecimal'
require 'spec_helper'
require 'rpn_calculator/input'

RSpec.describe RpnCalculator::Operations::Multiplication do
  describe '.apply' do
    subject(:apply) { described_class.apply(stack) }

    context 'given a stack with two numbers' do
      let(:stack) { [BigDecimal.new('2'), BigDecimal.new('3')] }

      it 'returns a stack with the result of the sum of the two numbers' do
        expect(apply).to eq([BigDecimal.new('6')])
      end
    end

    context 'given a stack with more than two numbers' do
      let(:stack) { [BigDecimal.new('1'), BigDecimal.new('4'), BigDecimal.new('5')] }

      it 'returns a stack with the sum and the rest of the original stack' do
        expect(apply).to eq([BigDecimal.new('1'), BigDecimal.new('20')])
      end
    end

    context 'given a stack with less than two numbers' do
      let(:stack) { [BigDecimal.new('1')] }

      it 'raises an ArgumentError' do
        expect { apply }.to raise_error ArgumentError
      end
    end
  end
end
