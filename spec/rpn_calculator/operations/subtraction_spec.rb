require 'bigdecimal'
require 'spec_helper'
require 'rpn_calculator/input'

RSpec.describe RpnCalculator::Operations::Subtraction do
  describe '.apply' do
    subject(:apply) { described_class.apply(stack) }

    context 'given a stack with two numbers' do
      let(:stack) { [BigDecimal.new('1'), BigDecimal.new('2')] }

      it 'returns a stack with the result of the subtraction of the two numbers' do
        expect(apply).to eq([BigDecimal.new('1')])
      end
    end

    context 'given a stack with more than two numbers' do
      let(:stack) { [BigDecimal.new('1'), BigDecimal.new('4'), BigDecimal.new('5')] }

      it 'returns a stack with the sum and the rest of the original stack' do
        expect(apply).to eq([BigDecimal.new('1'), BigDecimal.new('1')])
      end
    end

    context 'given a stack with less than two numbers' do
      let(:stack) { [BigDecimal.new('1')] }

      it 'raises an ArgumentError' do
        expect { apply }.to raise_error ArgumentError
      end
    end
  end
end
