require 'spec_helper'
require 'rpn_calculator/input'

RSpec.describe RpnCalculator::Operations::AddNumber do
  subject(:number) { described_class.new(value) }

  describe '#apply' do
    subject(:apply) { number.apply(stack) }
    let(:value) { 4 }
    let(:stack) { [1, 2, 3, 4, 5] }

    it 'adds the number value to the stack' do
      expect(apply).to eq([1, 2, 3, 4, 5, 4])
    end
  end
end
