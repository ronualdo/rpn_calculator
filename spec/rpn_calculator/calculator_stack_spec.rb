require 'spec_helper'
require 'rpn_calculator/calculator_stack'
require 'rpn_calculator/input'

RSpec.describe RpnCalculator::CalculatorStack do
  subject(:calculator_stack) { described_class.new(initial_stack) }

  describe '#add' do
    subject(:add_operation) { calculator_stack.add(operation) }
    let(:initial_stack) { [1, 2, 3] }
    let(:operation) { double(:operation) }

    context 'given an operation that removes the top element of the stack' do
      before do
        allow(operation).to receive(:apply)
          .with(initial_stack).and_return([1, 2])
      end

      it 'returns the top element of the stack' do
        expect(add_operation).to eq(2)
      end

      it 'replaces state by result of the operation apply' do
        add_operation
        
        expect(calculator_stack.state).to eq([1, 2])
      end
    end
  end
end
