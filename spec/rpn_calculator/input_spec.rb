require 'spec_helper'
require 'rpn_calculator/input'

RSpec.describe RpnCalculator::Input do
  describe '.parse' do
    subject(:parse) { described_class.parse(input) }

    context 'given a number input' do
      let(:input) { '1' }

      it 'returns a number input' do
        result = parse
        expect(result.value).to eq(1)
      end
    end

    context 'given an number with decimal value' do
      let(:input) { '1.5' }

      it 'returns a number input with a float value' do
        result = parse
        expect(result.value).to eq(1.5)
      end
    end

    context 'given a negative number' do
      let(:input) { '-1' }

      it 'returns a number input with the negative value' do
        result = parse
        expect(result.value).to eq(-1)
      end
    end

    context 'given an invalid input' do
      let(:input) { 'abc$1232' }

      it 'raises an ArgumentError' do
        expect { parse }.to raise_error ArgumentError
      end
    end

    context 'given a sum operation' do
      let(:input) { '+' }

      it 'returns a sum input' do
        expect(parse).to be_a_kind_of(RpnCalculator::Operations::Sum.class)
      end
    end

    context 'given a subtraction' do
      let(:input) { '-' }

      it { is_expected.to be_a_kind_of(RpnCalculator::Operations::Subtraction.class) }
    end

    context 'given a multiplication' do
      let(:input) { '*' }

      it { is_expected.to be_a_kind_of(RpnCalculator::Operations::Multiplication.class) }
    end

    context 'given a division' do
      let(:input) { '/' }

      it { is_expected.to be_a_kind_of(RpnCalculator::Operations::Division.class) }
    end
  end
end
