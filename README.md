# RpnCalculator

This gem implements a RPN calculator to be used for Unix users. Right now, it provides the
4 basic operators. I tried to keep a the architecture as simple as possible.

The app works with a stack where we add operations (Sum, Subtraction, Multiplication,
Division and Add Number) and each of these operations affects the state of the stack
and returning the operation result.

## Installation

After cloning the project, enter in its root folder:

```bash
cd rpn_calculator
```

In the project folder, you can install the application with the following command:

```bash
bundle exec rake install
```

## Usage

To start using the app, after installing it just run the command `rpn_calculator`. You
should see its prompt starting after that.
