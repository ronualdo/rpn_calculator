require 'rpn_calculator/version'
require 'rpn_calculator/input'
require 'rpn_calculator/calculator_stack'

module RpnCalculator
  class << self
    def process(input)
      operation = RpnCalculator::Input.parse(input)
      calculator_stack.add(operation)
    end

    private

    def calculator_stack
      @_calculator_stack ||= RpnCalculator::CalculatorStack.new([])
    end
  end
end
