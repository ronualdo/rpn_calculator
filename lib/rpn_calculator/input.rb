require 'rpn_calculator/operations'

module RpnCalculator
  module Input
    class << self
      def parse(input)
        raise ArgumentError, "invalid input: #{input}" unless valid_input?(input)
        fetch_operation(input) || RpnCalculator::Operations::AddNumber.new(input)
      end

      private

      def fetch_operation(input)
        RpnCalculator::Operations.fetch(input)
      end

      def valid_input?(input)
        number?(input) || RpnCalculator::Operations.include?(input)
      end

      def number?(input)
        input =~ /^(-)?\d(.)?\d*$/
      end
    end
  end
end
