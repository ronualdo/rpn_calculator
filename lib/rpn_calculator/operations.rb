require 'bigdecimal'

module RpnCalculator::Operations
  module Sum
    def self.apply(stack)
      RpnCalculator::Operations.run(stack) { |number1, number2| number1 + number2 }
    end
  end

  module Subtraction
    def self.apply(stack)
      RpnCalculator::Operations.run(stack) { |number1, number2| number1 - number2 }
    end
  end

  module Multiplication
    def self.apply(stack)
      RpnCalculator::Operations.run(stack) { |number1, number2| number1 * number2 }
    end
  end

  module Division
    def self.apply(stack)
      RpnCalculator::Operations.run(stack) { |number1, number2| number1 / number2 }
    end
  end

  class AddNumber
    attr_reader :value

    def initialize(value)
      @value = BigDecimal.new(value.to_s)
    end

    def apply(stack)
      stack_copy = stack.dup
      stack_copy.push(value)
    end
  end

  AVAILABLE_OPERATIONS = {
    '+' => Sum,
    '-' => Subtraction,
    '*' => Multiplication,
    '/' => Division
  }.freeze

  def self.run(stack)
    raise ArgumentError, "Can apply operation on #{stack}" if stack.size < 2
    stack_copy = stack.dup
    number1 = stack_copy.pop
    number2 = stack_copy.pop

    stack_copy.push yield(number1, number2)
  end

  def self.fetch(id)
    AVAILABLE_OPERATIONS[id]
  end

  def self.include?(id)
    AVAILABLE_OPERATIONS.keys.include?(id)
  end
end
