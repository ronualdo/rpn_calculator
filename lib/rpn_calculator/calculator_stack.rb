class RpnCalculator::CalculatorStack
  attr_reader :state

  def initialize(initial_stack)
    @state = initial_stack
  end

  def add(operation)
    @state = operation.apply(@state)
    @state.last
  end
end
