require 'rpn_calculator'

module RpnCalculator::Terminal
  class << self
    def run(_args)
      puts 'Running RPN Calculator'
      loop do
        print '> '
        input = read_input

        break unless input && input != 'q'
        puts handle_input(input)
      end
    end

    private

    def read_input
      input = gets
      input.chomp if input
    end

    def handle_input(input)
      RpnCalculator.process(input).to_s('F')
    rescue ArgumentError
      puts 'Illegal argument. Try something else'
    end
  end
end
